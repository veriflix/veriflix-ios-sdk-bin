# DO NOT EDIT! Edit VeriflixSDK.podspec.h instead


Pod::Spec.new do |spec|
  spec.name = "VeriflixSDK"
  spec.version = "1.7.23"
  spec.summary = "VeriflixSDK"
  spec.description = "SDK for access to media platform."
  spec.license = "MIT"
  spec.homepage = "https://veriflix.bitbucket.io/ios/"
  spec.authors = { "Ivan Zezyulya" => "ivan.zezyulya+veriflix@effective.band" }
  spec.cocoapods_version = '>= 1.9.0'
  spec.platform = :ios, "13.0"
  spec.source = { :path => '.' }
  spec.ios.frameworks = "MapKit", "CoreLocation"
  spec.swift_version = '5.1'
  spec.static_framework = true
  spec.vendored_frameworks = 'VeriflixSDK.xcframework'
  spec.pod_target_xcconfig = {
    'BUILD_LIBRARY_FOR_DISTRIBUTION' => 'YES'
  }

  spec.dependency 'ActionKit', '~> 2.5.3'
  spec.dependency 'AWSMobileClient', '~> 2.22.0'
  spec.dependency 'AWSS3', '~> 2.22.0'
  spec.dependency 'BugfenderSDK', '~> 1.10.0'
  spec.dependency 'JGProgressHUD', '~> 2.2.0'
  spec.dependency 'lottie-ios', '~> 2.5.3'
  spec.dependency 'Logging', '~> 1.4.0'
  spec.dependency 'R.swift'
  spec.dependency 'R.swift.Library'
  spec.dependency 'Sentry', '~> 6.0.11'
  spec.dependency 'SnapKit', '~> 5.0.1'
  spec.dependency 'TPKeyboardAvoiding', '~> 1.3.5'
  spec.dependency 'VideoEditorSDK', '~> 10.25.0'
end
