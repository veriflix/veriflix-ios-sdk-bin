#!/bin/bash -ex

PODSPEC='VeriflixSDK.podspec'

VERSION=$(cat $PODSPEC | grep 'spec.version' | perl -pe 's/\s*spec\.version = "(.*)"/$1/e')

git add -A
git commit -m "v$VERSION: "
git commit --amend

git tag $VERSION
